## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Infrastructure Map
![Alt text](terraform/images/infra-map.png?raw=true "ECS")

![Alt text](terraform/images/VPC.png?raw=true "VPC")

## Build Infrastructure

### Required
- Terraform v1.0.1 (https://www.terraform.io/downloads)
- Terraform is an open-source infrastructure as code software tool that enables you to safely and predictably create, change, and improve infrastructure.

### Start build infrastructure

#### Step 1
```bash
$ cd maplink_backend/terraform/dev
```
#### Step 2
- Edit the configuration information to build in the file ```terraform.tfvars```
- Edit bucket name, key, region, access_key, secret_key in the file ```main.tf``` 

#### Step 3
```bash
$ terraform init
```

#### Step 4
```bash
$ terraform apply
```

### Infrastructure informations

#### 1.VPC

```
| ID            | Name                     | Cdir           |
| ------------- | -------------------------| ---------------|
|               | vpc-maplink-service-dev  | 172.10.0.0/16  |
```

#### 2.Subnet

```
| Name                               | Network Address             |
| -----------------------------------| ----------------------------|
| private-subnet-maplink-service-dev | 172.10.1.0/24,172.10.2.0/24 |
| publicsubnet-maplink-service-dev   | 172.10.3.0/24,172.10.4.0/24 |
```

#### 3.Gateway

```
| ID            | Type              | Elastic IP | VPC                     |
| --------------| ------------------|------------| ------------------------|
|               | Internet gateways |            | vpc-maplink-service-dev |
|               | NAT gateways      |            | vpc-maplink-service-dev |
|               | NAT gateways      |            | vpc-maplink-service-dev |
```

#### 4.Security Group

```
| ID            | Name                            | VPC                      |
| ------------- | --------------------------------| -------------------------|
|               | rds-sg-maplink-service-dev      | vpc-maplink-service-dev  |
|               | ecs-task-sg-maplink-service-dev | vpc-maplink-service-dev  |
```

#### 5.ECS Cluster
```
| Name                        | Type       |
| ----------------------------| -----------|
| cluster-maplink-service-dev | FARGATE    |
```

#### 6.RDS Cluster
```
| Name                        | Engine                 | Size        | Instance                                                |
| ----------------------------| -----------------------| ------------|---------------------------------------------------------|
| cluster-maplink-service-dev | Aurora PostgreSQL      | 1 instance  | Name: cluster-maplink-service-dev-0, Size: db.t3.medium |
```

#### 7.RDS Instance
```
| Database Name        | Host                   | Port        |Master User           | Password     |
| ---------------------| -----------------------| ------------|----------------------|--------------|
| maplink              |                        | 5432        | maplink-service-user | aaRTBDNAaej! |
```

#### 8.ECR
```
| Name                          | URL        |
| ------------------------------| -----------|
| registry--maplink-service-dev |            |
```

#### 9.Load Balancers
```
| Name                          | DNS        | VPC                     |
| ------------------------------| -----------|-------------------------|
| alb--maplink-service-dev      |            | vpc-maplink-service-dev |
```


#### Note: Connect into RDS database and install extension PostGIS
```
CREATE EXTENSION postgis;
```

## Config CI/CD for deploy
- bitbucket-pipelines.yml (pipeline config for CI/CD)
```
- Enable for pipeline bitbucket
- Create environment with name DevelopBuild:

APP_PORT=
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

- Create environment with name DevelopBuild includes the following information:

CLUSTER_NAME=
SERVICE_NAME=
ECR_NAME=
SECURITY_GROUPS=
DESIRED_COUNT=
SUBNETS=
LOAD_BALANCER_LISTENER_ARN=
VPC_ID=
HEALTH_CHECK_PATH=
LISTENER_RULE_PRIORITY=
CONTAINER_PORT=
CPU_TASK=
MEMORY_TASK=

- Merge code to develop for trigger deploy service to ecs
```


## Installation source code for development

### Required
- docker (https://docs.docker.com/engine/install/ubuntu/)
- docker-composer (https://docs.docker.com/compose/install/)
- Build safer, share wider, run faster
### Running the app
```bash
$ cp .env.example .env

$ docker network create nestjs-server-network

$ docker-compose up -d
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
