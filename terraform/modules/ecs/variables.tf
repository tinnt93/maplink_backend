variable "ecs_cluster_name" {
    description = "The ECS cluster name."
    default = ""
}
variable "app_image" {
    description = "The docker image for app."
    default = ""
}
variable "app_port" {
    description = "The app port."
    default = 80
}
variable "fargate_cpu" {
    description = "The fargate cpu."
    default = ""
}
variable "fargate_memory" {
    description = "The fargate memory."
    default = ""
}
variable "aws_region" {
    description = "The aws region for task definition."
    default = ""
}
variable "cloudwatch_log_group_name" {
    description = "The cloudwatch log group for task definition."
    default = ""
}
variable "container_name" {
    description = "The container name"
    default     = ""
}
variable "ecs_task_execution_role_arn" {
    description = "The aws iam role ECS task execution."
    default = ""
}
variable "ecs_service_name" {
    description = "The ECS service name."
    default = ""
}
variable "number_containers" {
    description = "Number of docker containers to run for a service"
    default     = 1
}
variable "ecs_task_security_group_id" {
    description = "The aws security group for ecs tasks."
    default = ""
}
variable "private_subnet_ids" {
    description = "The aws subnet private ids."
    default = []
}
variable "alb_target_group_id" {
    description = "The aws alb target group id."
    default = ""
}
variable "ecs_task_name" {
    description = "The task name for ecs."
    default = ""
}
variable "is_build_ecs_service" {
    description = "The task name for ecs."
    default = true
}








