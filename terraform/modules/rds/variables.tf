variable "identifier" {
    description = "The name of the RDS instance"
    default     = ""
}
variable "db_subnet_group_name" {
    description = "The name of the DB subnet group"
    default     = ""
}
variable "private_subnet_ids" {
    description = "A list of VPC subnet IDs"
    default     = []
}
variable "allocated_storage" {
    description = "The allocated storage in gibibytes"
    default     = ""
}
variable "engine" {
    description = "The database engine to use."
    default     = ""
}
variable "engine_version" {
    description = "(Optional) The engine version to use"
    default     = ""
}
variable "instance_class" {
    description = "The instance type of the RDS instance."
    default     = ""
}
variable "db_name" {
    description = "The database name."
    default     = ""
}
variable "username" {
    description = "The username."
    default     = ""
}
variable "password" {
    description = "The password."
    default     = ""
}
variable "database_authentication_enabled" {
    description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled."
    default     = false
}
variable "rds_security_group_id" {
    description = "The RDS security group id."
    default     = ""
}
variable "kms_key_id" {
    description = "The ARN for the KMS encryption key"
    default     = ""
}

