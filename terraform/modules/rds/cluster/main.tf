
resource "aws_db_subnet_group" "default" {
    name        = var.db_subnet_group_name
    subnet_ids  = var.private_subnet_ids
}

resource "aws_rds_cluster_instance" "cluster_instances" {
    count                           = var.number_cluster_instance
    identifier                      = "${var.identifier}-${count.index}"
    cluster_identifier              = aws_rds_cluster.default.id
    instance_class                  = var.instance_class
    engine                          = aws_rds_cluster.default.engine
    engine_version                  = aws_rds_cluster.default.engine_version
}


resource "aws_rds_cluster" "default" {
    cluster_identifier                  = var.identifier
    database_name                       = var.db_name
    master_username                     = var.username
    master_password                     = var.password
    engine                              = var.engine
    engine_version                      = var.engine_version
    db_subnet_group_name                = aws_db_subnet_group.default.id
    skip_final_snapshot                 = var.skip_final_snapshot
    storage_encrypted                   = "true"
    vpc_security_group_ids              = [var.rds_security_group_id]
    # kms_key_id                          = var.is_kms_key == true ? var.kms_key_id : null
    iam_database_authentication_enabled = var.database_authentication_enabled
    backup_retention_period             = var.backup_retention_period
    preferred_backup_window             = var.preferred_backup_window
    copy_tags_to_snapshot               = true
}