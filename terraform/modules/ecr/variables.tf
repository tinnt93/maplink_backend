variable "ecr_name" {
    description = "The name of the repository."
    default = ""
}
variable "image_tag_mutability" {
    description = "The tag mutability setting for the repository. Must be one of: MUTABLE or IMMUTABLE. Defaults to MUTABLE"
    default = "MUTABLE"
}
variable "scan_on_push" {
    description = "Configuration block that defines image scanning configuration for the repository"
    default = false
}