output "elasticache_security_group_id" {
    description = "The elasticache security group id"
    value       = aws_security_group.elasticache.id
}