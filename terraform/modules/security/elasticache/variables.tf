variable "elasticache_security_group_name" {
    description = "The name elasticache security group"
    default     = ""
}
variable "elasticache_security_group_description" {
    description = "The description security group"
    default     = ""
}
variable "vpc_id" {
    description = "The VPC ID."
    default     = ""
}
variable "elasticache_port" {
    description = "The ingress elasticache port."
    default     = ""
}
variable "vpc_cidr_block" {
    description = "The ingress elasticache port."
    default     = ""
}
