provider "aws" {
    region     = var.aws_region
    access_key = var.aws_access_key
    secret_key = var.aws_secret_key
}

terraform {
    backend "s3" {
        bucket = "terraform-state-dev"
        key    = "terraform.tfstate"
        region = ""
        access_key = ""
        secret_key = ""
    }
}

# Create Network and VPC
module "network" {
    source              = "../modules/network"
    aws_vpc_cidr_block  = var.vpc_cidr_block
    az_count            = var.az_count
    vpc_name            = "vpc-${var.project_name}-${var.env}"
    subnet_private_name = "private-subnet-${var.project_name}-${var.env}"
    subnet_public_name  = "public-subnet-${var.project_name}-${var.env}"
}

# Create Security group
module "security" {
    source                               = "../modules/security"
    alb_security_group_name              = "alb-sg-${var.project_name}-${var.env}"
    alb_security_group_description       = var.alb_security_group_description
    vpc_id                               = module.network.vpc_id
    app_port                             = var.app_port
    ecs_tasks_security_group_name        = "ecs-task-sg-${var.project_name}-${var.env}"
    ecs_tasks_security_group_description = var.ecs_tasks_security_group_description
    vpc_cidr_block                       = module.network.vpc_cidr_block
}

# Create cert for attach for ALB
module "alb_cert" {
    source                    = "../modules/certificate"
    route53_zone_name         = var.route53_zone_name
    route53_private_zone      = var.route53_private_zone
    certificate_domain_name   = var.alb_certificate_domain_name
    subject_alternative_names = var.alb_subject_alternative_names
}

# Create ALB
module "alb" {
    source                              = "../modules/alb"
    alb_name                            = "alb-${var.project_name}-${var.env}"
    aws_public_subnets                  = module.network.public_subnet_ids
    aws_alb_security_group_id           = module.security.alb_security_group_id
    alb_target_group_name               = "alb-tg-${var.project_name}-${var.env}"
    vpc_id                              = module.network.vpc_id
    health_check_path                   = var.health_check_path
    app_port                            = var.app_port
    is_ssl                              = var.is_ssl
    certificate_arn_attach_alb_listener = module.alb_cert.acm_certificate_arn
}

# Create ECS cluster and task definition
module "ecs" {
    source                          = "../modules/ecs"
    is_build_ecs_service            = var.is_build_ecs_service
    ecs_cluster_name                = "cluster-${var.project_name}-${var.env}"
}

# Create RDS security group
module "rds_security" {
    source                               = "../modules/security/rds"
    vpc_id                               = module.network.vpc_id
    rds_security_group_name              = "rds-sg-${var.project_name}-${var.env}"
    rds_security_group_description       = var.rds_security_group_description
    rds_port                             = var.rds_port
    vpc_cidr_block                       = module.network.vpc_cidr_block
}

# Create EC2 bastion security group
module "ec2_bastion_security" {
    source                               = "../modules/security/ec2"
    vpc_id                               = module.network.vpc_id
    ec2_security_group_name              = "bastion-ec2-sg-${var.project_name}-${var.env}"
    ec2_security_group_description       = var.ec2_security_group_description
    ingress_ssh_port                     = var.ec2_bastion_ssh_port
}

# Create ECR for gateway service
module "registry" {
    source               = "../modules/ecr"
    ecr_name             = "registry-${var.project_name}-${var.env}"
    image_tag_mutability = var.image_tag_mutability
    scan_on_push         = var.scan_on_push
}

# Create RDS
module "rds" {
    source                               = "../modules/rds/cluster"
    db_subnet_group_name                 = "rds-subnet-group-${var.project_name}-${var.env}"
    private_subnet_ids                   = module.network.private_subnet_ids
    identifier                           = "cluster-${var.project_name}-${var.env}"
    allocated_storage                    = var.allocated_storage
    engine                               = var.engine
    engine_version                       = var.engine_version
    instance_class                       = var.instance_class
    db_name                              = var.db_name
    username                             = var.username
    password                             = var.password
    database_authentication_enabled      = var.database_authentication_enabled
    rds_security_group_id                = module.rds_security.rds_security_group_id
    kms_key_id                           = var.kms_key_id
    availability_zones                   = var.availability_zones
    backup_retention_period              = var.backup_retention_period
    preferred_backup_window              = var.preferred_backup_window
    skip_final_snapshot                  = var.skip_final_snapshot
    number_cluster_instance              = var.number_cluster_instance
    is_kms_key                           = var.is_kms_key
}

# Key pair
module "key_pair" {
    source     = "../modules/key_pair"
    key_name   = "${var.project_name}-${var.env}"
}

# EC2 bastion
module "ec2" {
    source               = "../modules/ec2"
    ec2_instance_name    = "bastion-${var.project_name}-${var.env}"
    ami                  = var.ami
    security_group_id    = module.ec2_bastion_security.ssh_security_group_id
    key_pair_name        = module.key_pair.key_name
    instance_type        = var.instance_type
    number_instances     = var.number_instances
    private_subnet_ids   = module.network.public_subnet_ids
    ssh_port             = var.ec2_bastion_ssh_port
}
