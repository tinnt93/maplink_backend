import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [
        TypeOrmModule.forRoot(),
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        UserModule
    ],
    controllers: [AppController],
    providers: [],
})
export class AppModule {}
