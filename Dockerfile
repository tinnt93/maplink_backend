FROM node:14-alpine As development

ARG path=/home/node/app
RUN mkdir -p $path
WORKDIR $path
COPY package*.json ./
RUN npm install --only=development
COPY . .
RUN npm run build

FROM node:14-alpine As production

ARG path=/home/node/app
ARG NODE_ENV=production
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV NODE_ENV=${NODE_ENV}

RUN mkdir -p $path
WORKDIR $path

COPY package*.json ./
RUN npm install --only=production
COPY . .
COPY --from=development /home/node/app/dist ./dist

CMD ["node", "dist/main"]